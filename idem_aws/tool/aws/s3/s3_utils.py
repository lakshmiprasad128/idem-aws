"""Tool module for utility functions for Amazon S3 bucket."""
from typing import Dict
from typing import List

from dict_tools import data


def get_notification_configuration_diff(
    hub, new_notifications: Dict[str, List], old_state: Dict[str, List]
) -> Dict[str, List]:
    """This functions helps in comparing two dicts.

    It compares each key value in both the dicts and return diff notifications configuration.

    Args:
        new_notifications(Dict[str, List]):
            New notifications.

        old_state(Dict[str, List]):
            Old Idem state used to retrieve old notifications.

    Returns:
        {Resultant notifications dictionary}
    """
    old_state_notification = old_state.get("notifications")

    old_state_notification = (
        old_state_notification if old_state_notification is not None else {}
    )

    return data.recursive_diff(
        new_notifications, old_state_notification, ignore_order=True
    )


def get_create_bucket_payload(
    hub,
    acl,
    name,
    create_bucket_configuration,
    grant_full_control,
    grant_read,
    grant_read_acp,
    grant_write,
    grant_write_acp,
    object_lock_enabled_for_bucket,
    object_ownership,
) -> Dict[str, object]:
    """This functions helps in creating payload to create bucket.

    Returns:
        {payload dictionary}
    """
    payload = {"Bucket": name}
    if acl is not None:
        payload["ACL"] = acl
    if create_bucket_configuration is not None:
        payload["CreateBucketConfiguration"] = create_bucket_configuration
    if grant_full_control is not None:
        payload["GrantFullControl"] = grant_full_control
    if grant_read is not None:
        payload["GrantRead"] = grant_read
    if grant_read_acp is not None:
        payload["GrantReadACP"] = grant_read_acp
    if grant_write is not None:
        payload["GrantWrite"] = grant_write
    if grant_write_acp is not None:
        payload["GrantWriteACP"] = grant_write_acp
    if object_lock_enabled_for_bucket is not None:
        payload["ObjectLockEnabledForBucket"] = object_lock_enabled_for_bucket
    if object_ownership is not None:
        payload["ObjectOwnership"] = object_ownership
    return payload


def get_grantee_payload(hub, permission, grantee) -> str:
    """This functions helps in creating payload of grantee.

    Returns:
        {grantee string}
    """
    if permission:
        delimiter = ", "
    else:
        delimiter = ""
    if "ID" in grantee.keys():
        permission += delimiter + "ID=" + grantee["ID"]
    if "EmailAddress" in grantee.keys():
        permission += delimiter + "EmailAddress=" + grantee["EmailAddress"]
    if "URI" in grantee.keys():
        permission += delimiter + "URI=" + grantee["URI"]
    return permission
