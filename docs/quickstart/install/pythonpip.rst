Python Pip
==========

Idem can be pip installed into an existing Python installation.
Python 3.7 is the minimum version supported by Idem.

Verify you have Python 3.7 or later installed:

.. code-block:: bash

    python -V


Once you've verified you have a supported version of Python run the following
to install Idem.

.. code-block:: bash

    pip3 install idem


Check your version of Idem.

.. code-block:: bash

    idem --version


Now install `idem-aws` like this:


.. code-block:: bash

   idem pip install idem-aws

.. include:: ./_includes/install_idem-aws.rst
