Linux
=====

Choose your OS
++++++++++++++

.. tab-set::

    .. tab-item:: RedHat/CentOS
        :name: rpm

        * You might need to install these utilities.

          .. code-block:: bash

             yum install curl unzip

        * Download the latest RPM from https://repo.idemproject.io/idem/latest/linux/rpm/

          .. code-block:: bash

              curl -O https://repo.idemproject.io/idem/latest/linux/rpm/idem-<current version>.rpm

        * Install rpm:

          .. code-block:: bash

              yum install ./idem*.rpm

    .. tab-item:: Debian/Ubuntu
        :name: deb

        * You might need to install these utilities.

          .. code-block:: bash

             apt-get install curl unzip

        * Download the latest DEB file from https://repo.idemproject.io/idem/latest/linux/deb/

          .. code-block:: bash

              curl -O https://repo.idemproject.io/idem/latest/linux/deb/idem_<current version>.deb

        * Install deb:

          .. code-block:: bash

              apt-get install ./idem*.deb



Now you're ready to start using Idem!

Check your version of idem.

.. code-block:: bash

    idem --version

Now install `idem-aws` like this:


.. code-block:: bash

   idem pip install idem-aws


.. include:: ./_includes/install_idem-aws.rst
