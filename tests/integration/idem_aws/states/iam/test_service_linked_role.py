import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_service_linked_role(hub, ctx):
    custom_suffix = str(int(time.time()))
    temp_name = "AWSServiceRoleForAutoScaling_" + custom_suffix
    service_name = "autoscaling.amazonaws.com"
    description = "This is test description"
    tags_to_add = {"firstKey": "firstValue", "2ndKey": "2ndValue"}
    tags_to_update = {
        "3rdKey": "3rdValue",
        "4thKey": "4thValue",
        "firstKey": "firstValue",
        "2ndKey": "2ndNewValue",
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # create service-linked-role with test flag
    ret = await hub.states.aws.iam.service_linked_role.present(
        test_ctx,
        name=temp_name,
        service_name=service_name,
        custom_suffix=custom_suffix,
        description=description,
        tags=tags_to_add,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.iam.service_linked_role '{temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state_resource = ret.get("new_state")
    assert temp_name == new_state_resource.get("name")
    assert temp_name == new_state_resource.get("resource_id")
    assert service_name == new_state_resource.get("service_name")
    assert custom_suffix == new_state_resource.get("custom_suffix")
    assert description == new_state_resource.get("description")
    assert tags_to_add == new_state_resource.get("tags")

    # create service-linked-role in real
    ret = await hub.states.aws.iam.service_linked_role.present(
        ctx,
        name=temp_name,
        service_name=service_name,
        custom_suffix=custom_suffix,
        description=description,
        tags=tags_to_add,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.iam.service_linked_role '{temp_name}'" in ret["comment"]
    new_state_resource = ret.get("new_state")
    created_resource_name = new_state_resource.get("name")
    assert temp_name == new_state_resource.get("name")
    assert temp_name == new_state_resource.get("resource_id")
    assert service_name == new_state_resource.get("service_name")
    assert custom_suffix == new_state_resource.get("custom_suffix")
    assert description == new_state_resource.get("description")
    assert tags_to_add == new_state_resource.get("tags")

    # describe service-linked-role
    describe_ret = await hub.states.aws.iam.service_linked_role.describe(ctx)
    assert describe_ret
    assert "aws.iam.service_linked_role.present" in describe_ret.get(
        "iam-service_linked_role-" + created_resource_name
    )
    described_resource = describe_ret.get(
        "iam-service_linked_role-" + created_resource_name
    ).get("aws.iam.service_linked_role.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert temp_name == described_resource_map["name"]
    assert "resource_id" in described_resource_map
    assert temp_name == described_resource_map["resource_id"]
    assert "service_name" in described_resource_map
    assert service_name == described_resource_map["service_name"]
    assert "custom_suffix" in described_resource_map
    assert custom_suffix == described_resource_map["custom_suffix"]
    assert "description" in described_resource_map
    assert description == described_resource_map["description"]
    assert "tags" in described_resource_map
    assert tags_to_add == described_resource_map["tags"]

    # Update service-linked-role with test flag
    ret = await hub.states.aws.iam.service_linked_role.present(
        test_ctx,
        name=temp_name,
        resource_id=temp_name,
        service_name=service_name,
        custom_suffix=custom_suffix,
        description=description,
        tags=tags_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        "Would update tags: Add keys dict_keys(['2ndKey', '3rdKey', '4thKey']) Remove keys dict_keys(['2ndKey'])"
        in ret["comment"]
    )
    update_new_state = ret.get("new_state")
    assert temp_name == update_new_state.get("name")
    assert temp_name == update_new_state.get("resource_id")
    assert service_name == update_new_state.get("service_name")
    assert custom_suffix == update_new_state.get("custom_suffix")
    assert description == update_new_state.get("description")
    assert tags_to_update == update_new_state.get("tags")

    # update service-linked-role in real
    ret = await hub.states.aws.iam.service_linked_role.present(
        ctx,
        name=temp_name,
        resource_id=temp_name,
        service_name=service_name,
        custom_suffix=custom_suffix,
        description=description,
        tags=tags_to_update,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        "Update tags: Add keys dict_keys(['2ndKey', '3rdKey', '4thKey']) Remove keys dict_keys(['2ndKey'])"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert temp_name == update_new_state.get("name")
    assert temp_name == update_new_state.get("resource_id")
    assert service_name == update_new_state.get("service_name")
    assert custom_suffix == update_new_state.get("custom_suffix")
    assert description == update_new_state.get("description")
    assert tags_to_update == update_new_state.get("tags")

    # Delete service-linked-role with test flag
    ret = await hub.states.aws.iam.service_linked_role.absent(
        test_ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == created_resource_name
    assert ret["old_state"]["tags"] == resource.get("tags")
    assert (
        f"Would delete aws.iam.service_linked_role '{created_resource_name}'"
        in ret["comment"]
    )

    # Delete service-linked-role in real
    ret = await hub.states.aws.iam.service_linked_role.absent(
        ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["old_state"]["name"] == created_resource_name
    assert ret["old_state"]["tags"] == resource.get("tags")
    assert (
        f"Deleted aws.iam.service_linked_role '{created_resource_name}'"
        in ret["comment"]
    )

    # Deleting service-linked-role again should be a no-op
    ret = await hub.states.aws.iam.service_linked_role.absent(
        ctx, name=temp_name, resource_id=temp_name
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.iam.service_linked_role '{created_resource_name}' already absent"
        in ret["comment"]
    )
