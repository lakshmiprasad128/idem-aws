import time
from collections import ChainMap
from typing import Any
from typing import Dict
from typing import List

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-elbv2-listener-" + str(int(time.time())),
    "port": 80,
    "protocol": "HTTP",
}
RESOURCE_TYPE = "aws.elbv2.listener"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    aws_elbv2_load_balancer,
    aws_elbv2_target_group,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    PARAMETER["lb_arn"] = aws_elbv2_load_balancer.get("resource_id")
    PARAMETER["default_actions"] = [
        {
            "Type": "forward",
            "TargetGroupArn": aws_elbv2_target_group.get("resource_id"),
            "ForwardConfig": {
                "TargetGroups": [
                    {
                        "TargetGroupArn": aws_elbv2_target_group.get("resource_id"),
                        "Weight": 1,
                    }
                ],
                "TargetGroupStickinessConfig": {"Enabled": False},
            },
        }
    ]
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    response = await hub.states.aws.elbv2.listener.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert PARAMETER["name"] == resource.get("name")
    assert_listener(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-resource_id", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    # This test  is here to avoid creating another ElasticLoadBalancingv2 Listener fixture for exec.get() testing.
    ret = await hub.exec.aws.elbv2.listener.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert_listener(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-lb-arn", depends=["exec-get-by-resource_id"])
async def test_exec_get_by_lb_resource_id(hub, ctx):
    # This test  is here to avoid creating another ElasticLoadBalancingv2 Listener fixture for exec.get() testing.
    ret = await hub.exec.aws.elbv2.listener.get(
        ctx,
        name=PARAMETER["name"],
        load_balancer_arn=PARAMETER["lb_arn"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert_listener(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-listener", depends=["exec-get-by-lb-arn"])
async def test_update(
    hub,
    ctx,
    __test,
    aws_certificate_manager_import,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    PARAMETER["tags"] = {
        "Name": PARAMETER["name"],
        "Description": "ELBv2 Listener.",
    }
    PARAMETER["port"] = 443
    PARAMETER["protocol"] = "HTTPS"
    PARAMETER["default_certificates"] = [
        {
            "CertificateArn": aws_certificate_manager_import.get("resource_id"),
        },
    ]
    PARAMETER["ssl_policy"] = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
    response = await hub.states.aws.elbv2.listener.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    resource = response.get("new_state")
    assert PARAMETER["ssl_policy"] == resource.get("ssl_policy")
    assert PARAMETER["name"] == resource.get("name")
    compare_certificates(
        PARAMETER["default_certificates"], resource.get("default_certificates")
    )
    assert_listener(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-certificates", depends=["update-listener"])
async def test_update_tags_and_certificates(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    PARAMETER["tags"] = {
        "Description": "ELBv2 Listener. Updated",
        "Summary": "Summary of ELBv2 Listener",
    }
    PARAMETER["ssl_policy"] = "ELBSecurityPolicy-FS-1-2-Res-2019-08"
    response = await hub.states.aws.elbv2.listener.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    resource = response.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["ssl_policy"] == resource.get("ssl_policy")
    compare_certificates(
        PARAMETER["default_certificates"], resource.get("default_certificates")
    )
    assert_listener(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update-certificates"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.elbv2.listener.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.elbv2.listener.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.elbv2.listener.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert_listener(resource, PARAMETER)
    compare_certificates(
        PARAMETER["default_certificates"], resource.get("default_certificates")
    )
    assert PARAMETER["ssl_policy"] == resource.get("ssl_policy")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    ret = await hub.exec.aws.elbv2.listener.get(
        ctx, name=PARAMETER["name"], resource_id="invalid-xyz-listener-arn"
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeListeners"
        f" operation: 'invalid-xyz-listener-arn' is not a valid listener ARN"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_lb_resource_id(hub, ctx):
    ret = await hub.exec.aws.elbv2.listener.get(
        ctx,
        name=PARAMETER["name"],
        load_balancer_arn="invalid-xyz-lb-arn",
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeListeners"
        f" operation: 'invalid-xyz-lb-arn' is not a valid load balancer ARN"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete AWS ElasticLoadBalancing(ELB) Listener.
    ret = await hub.states.aws.elbv2.listener.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert_listener(resource, PARAMETER)
    compare_certificates(
        PARAMETER["default_certificates"], resource.get("default_certificates")
    )
    assert PARAMETER["ssl_policy"] == resource.get("ssl_policy")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get-already-absent", depends=["absent"])
async def test_get_already_deleted(hub, ctx):
    ret = await hub.exec.aws.elbv2.listener.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ListenerNotFoundException: An error occurred (ListenerNotFound) when calling the DescribeListeners"
        f" operation: One or more listeners not found" in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elbv2.listener.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_lb_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.elbv2.listener.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_lb_absent_with_invalid_resource_id(hub, ctx):
    ret = await hub.states.aws.elbv2.listener.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid-xyz-abc",
    )
    assert not ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeListeners operation: "
        f"'invalid-xyz-abc' is not a valid listener ARN" in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
@pytest.mark.localstack(False)
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.elbv2.listener.absent(
            ctx, name=PARAMETER["name"], load_balancer_arn=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_listener(resource, parameters):
    assert resource.get("tags")
    assert parameters["tags"] == resource.get("tags")
    assert parameters["port"] == resource.get("port")
    assert parameters["protocol"] == resource.get("protocol")
    assert parameters["lb_arn"] == resource.get("lb_arn")
    assert resource.get("default_actions")
    assert len(resource.get("default_actions")) == len(parameters["default_actions"])
    assert parameters["default_actions"][0] == resource.get("default_actions")[0]


def compare_certificates(
    input_certificates: List[Dict[str, Any]], output_certificates: List[Dict[str, Any]]
):
    r"""
    Compares input and output certificate lists and return True/ False.
    Returns:
        True/ False
    """
    assert output_certificates
    assert len(input_certificates) == len(output_certificates)
    out_certificates_map = {
        certificate.get("CertificateArn"): certificate
        for certificate in output_certificates or []
    }
    for certificate in input_certificates:
        if certificate.get("CertificateArn") in out_certificates_map:
            if certificate != out_certificates_map.get(
                certificate.get("CertificateArn")
            ):
                return False
    return True
