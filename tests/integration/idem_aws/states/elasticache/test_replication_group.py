import copy
import time
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-replication-group-" + str(int(time.time())),
    "replication_group_description": "idem-test-replication-description",
    "engine": "Redis",
    "automatic_failover_enabled": False,
    "snapshot_retention_limit": 0,
    "cache_node_type": "cache.t2.micro",
    "transit_encryption_enabled": False,
    "at_rest_encryption_enabled": False,
}
STATE_NAME = "aws.elasticache.replication_group"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER, STATE_NAME
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    ret = await hub.states.aws.elasticache.replication_group.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_distribution(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.elasticache.replication_group.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.elasticache.replication_group.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.elasticache.replication_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_distribution(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create an replication group fixture for exec.get() testing
async def test_exec_get(hub, ctx, __test):
    ret = await hub.exec.aws.elasticache.replication_group.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert_distribution(PARAMETER, resource)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get_with_invalid_resource_id", depends=["present"])
async def test_get_with_invalid_resource_id(hub, ctx, __test):
    ret = await hub.exec.aws.elasticache.replication_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.elasticache.replication_group", name=PARAMETER["name"]
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter[
        "replication_group_description"
    ] = "idem-test-replication-description-new"
    new_parameter["tags"] = {
        f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
    }
    ret = await hub.states.aws.elasticache.replication_group.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert_distribution(old_resource, PARAMETER)
    resource = ret["new_state"]
    assert_distribution(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elasticache.replication_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert_distribution(old_resource, PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elasticache.replication_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_replication_group_absent_with_none_resource_id(hub, ctx):
    replication_group_temp_name = "idem-test-replication_group-" + str(uuid.uuid4())
    # Delete replication_group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.elasticache.replication_group.absent(
        ctx, name=replication_group_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=replication_group_temp_name
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.elasticache.replication_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_distribution(resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("replication_group_description") == resource.get(
        "replication_group_description"
    )
    assert parameters.get("cache_node_type") == resource.get("cache_node_type")
    assert parameters.get("tags") == resource.get("tags")
    assert parameters.get("automatic_failover_enabled") == resource.get(
        "automatic_failover_enabled"
    )
    assert parameters.get("snapshot_retention_limit") == resource.get(
        "snapshot_retention_limit"
    )
    assert parameters.get("transit_encryption_enabled") == resource.get(
        "transit_encryption_enabled"
    )
    assert parameters.get("at_rest_encryption_enabled") == resource.get(
        "at_rest_encryption_enabled"
    )
