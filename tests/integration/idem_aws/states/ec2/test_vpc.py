import copy
import os
import random
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-vpc-" + str(int(time.time())),
    "instance_tenancy": "default",
    "enable_dns_hostnames": True,
    "enable_dns_support": True,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    cidr_block = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK")
    if cidr_block is None:
        cidr_block = f"192.168.{random.randint(0, 255)}.0/24"
    PARAMETER["cidr_block_association_set"] = [{"CidrBlock": cidr_block}]
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.ec2.vpc '{PARAMETER['name']}'" in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert f"Created aws.ec2.vpc '{PARAMETER['name']}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["instance_tenancy"] == resource.get("instance_tenancy")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["enable_dns_hostnames"] == resource.get("enable_dns_hostnames")
    assert PARAMETER["enable_dns_support"] == resource.get("enable_dns_support")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.vpc.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.vpc.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.vpc.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "cidr_block_association_set" in described_resource_map
    assert (
        PARAMETER["cidr_block_association_set"][0]["CidrBlock"]
        == described_resource_map["cidr_block_association_set"][0]["CidrBlock"]
    )
    assert PARAMETER["instance_tenancy"] == described_resource_map.get(
        "instance_tenancy"
    )
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["enable_dns_support"] == described_resource_map.get(
        "enable_dns_support"
    )
    assert PARAMETER["enable_dns_hostnames"] == described_resource_map.get(
        "enable_dns_hostnames"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="add_cidr_tag", depends=["describe"])
async def test_add_cidr_tag(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    cidr_block_2 = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK_2")
    if cidr_block_2 is None:
        cidr_block_2 = f"192.168.{random.randint(0, 255)}.0/24"
    new_parameter["cidr_block_association_set"].append({"CidrBlock": cidr_block_2})
    new_parameter["tags"].update(
        {
            f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
        }
    )
    new_parameter["enable_dns_support"] = False
    ret = await hub.states.aws.ec2.vpc.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["cidr_block_association_set"][0]["CidrBlock"] == old_resource[
        "cidr_block_association_set"
    ][0].get("CidrBlock")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["instance_tenancy"] == old_resource.get("instance_tenancy")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["enable_dns_hostnames"] == old_resource.get("enable_dns_hostnames")
    assert PARAMETER["enable_dns_support"] == old_resource.get("enable_dns_support")
    resource = ret["new_state"]
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 2 == len(real_cidr_blocks)
    assert PARAMETER["cidr_block_association_set"][0]["CidrBlock"] in real_cidr_blocks
    assert (
        new_parameter["cidr_block_association_set"][0]["CidrBlock"] in real_cidr_blocks
    )
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["enable_dns_hostnames"] == resource.get("enable_dns_hostnames")
    assert new_parameter["enable_dns_support"] == resource.get("enable_dns_support")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="remove_cidr_tag", depends=["add_cidr_tag"])
async def test_remove_cidr_tag(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"] = {"Name": new_parameter["tags"]["Name"]}
    new_parameter["cidr_block_association_set"] = [
        new_parameter["cidr_block_association_set"][0]
    ]
    ret = await hub.states.aws.ec2.vpc.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["cidr_block_association_set"][0]["CidrBlock"] == old_resource[
        "cidr_block_association_set"
    ][0].get("CidrBlock")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["instance_tenancy"] == old_resource.get("instance_tenancy")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["enable_dns_hostnames"] == old_resource.get("enable_dns_hostnames")
    assert PARAMETER["enable_dns_support"] == old_resource.get("enable_dns_support")
    resource = ret["new_state"]
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 1 == len(real_cidr_blocks)
    assert (
        new_parameter["cidr_block_association_set"][0]["CidrBlock"] in real_cidr_blocks
    )
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["enable_dns_hostnames"] == resource.get("enable_dns_hostnames")
    assert new_parameter["enable_dns_support"] == resource.get("enable_dns_support")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["remove_cidr_tag"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["cidr_block_association_set"][0]["CidrBlock"] == old_resource[
        "cidr_block_association_set"
    ][0].get("CidrBlock")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["instance_tenancy"] == old_resource.get("instance_tenancy")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["enable_dns_hostnames"] == old_resource.get("enable_dns_hostnames")
    assert PARAMETER["enable_dns_support"] == old_resource.get("enable_dns_support")
    if __test:
        assert f"Would delete aws.ec2.vpc '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.ec2.vpc '{PARAMETER['name']}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.vpc '{PARAMETER['name']}' already absent" in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_vpc_absent_with_none_resource_id(hub, ctx):
    vpc_temp_name = "idem-test-vpc-" + str(int(time.time()))
    # Delete vpc with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=vpc_temp_name, resource_id=None)
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.vpc '{vpc_temp_name}' already absent" in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.vpc.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
