import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_cluster(
    hub,
    ctx,
    aws_iam_cluster_role_assignment,
    aws_ec2_subnet_for_eks_worker,
    aws_ec2_subnet_for_eks,
    aws_security_group,
    aws_kms_key,
):
    # create, update or delete of cluster is not supported in localstack. so using real aws to test.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    arn = aws_iam_cluster_role_assignment["arn"]
    cluster_name = "idem-test-cluster-" + str(uuid.uuid4())
    version = "1.20"
    resources_vpc_config = {
        "endpointPrivateAccess": True,
        "endpointPublicAccess": False,
        "subnetIds": [
            aws_ec2_subnet_for_eks_worker.get("resource_id"),
            aws_ec2_subnet_for_eks.get("resource_id"),
        ],
        "securityGroupIds": [aws_security_group.get("resource_id")],
    }
    kubernetes_network_config = {
        "ipFamily": "ipv4",
        "serviceIpv4Cidr": "172.20.0.0/16",
    }
    tags = {"Name": cluster_name}
    logging = {
        "clusterLogging": [
            {
                "enabled": True,
                "types": [
                    "api",
                    "audit",
                    "authenticator",
                    "controllerManager",
                    "scheduler",
                ],
            }
        ]
    }
    encryption_config = [
        {
            "provider": {"keyArn": aws_kms_key.get("arn")},
            "resources": ["secrets"],
        }
    ]

    # Create eks cluster with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await hub.states.aws.eks.cluster.present(
        test_ctx,
        name=cluster_name,
        role_arn=arn,
        version=version,
        resources_vpc_config=resources_vpc_config,
        kubernetes_network_config=kubernetes_network_config,
        logging=logging,
        encryption_config=encryption_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_cluster(
        resource,
        cluster_name,
        arn,
        version,
        resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        tags,
    )

    # create cluster in real
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version=version,
        resources_vpc_config=resources_vpc_config,
        kubernetes_network_config=kubernetes_network_config,
        logging=logging,
        encryption_config=encryption_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    resource = await is_cluster_active(hub, ctx, resource_id, cluster_name)
    assert_cluster(
        resource,
        cluster_name,
        arn,
        version,
        resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        tags,
    )

    # Describe cluster
    describe_ret = await hub.states.aws.eks.cluster.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.eks.cluster.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.eks.cluster.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_cluster(
        described_resource_map,
        cluster_name,
        arn,
        version,
        resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        tags,
    )
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.eks.cluster.present"], "tags", tags
    )

    # Test exec get cluster
    get_res = await hub.exec.aws.eks.cluster.get(
        ctx, name=cluster_name, resource_id=resource_id
    )
    assert get_res["result"], get_res["comment"]
    assert get_res.get("ret")
    assert_cluster(
        get_res.get("ret"),
        cluster_name,
        arn,
        version,
        resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        tags,
    )

    # Test exec get cluster when cluster does not exist
    non_existent_cluster_name = "dummy"
    get_ret = await hub.exec.aws.eks.cluster.get(
        ctx, name=non_existent_cluster_name, resource_id=non_existent_cluster_name
    )
    assert not get_ret["result"], get_ret["comment"]
    assert not get_ret.get("ret")

    # updating cluster configuration
    new_logging = {
        "clusterLogging": [
            {"enabled": True, "types": ["api", "scheduler"]},
            {
                "enabled": False,
                "types": ["audit", "authenticator", "controllerManager"],
            },
        ]
    }
    new_version = "1.21"
    new_tags = {
        "Name": cluster_name,
        f"idem-test-cluster-key-{str(uuid.uuid4())}": f"idem-test-cluster-value-{str(uuid.uuid4())}",
    }
    new_resources_vpc_config = {
        "endpointPrivateAccess": False,
        "endpointPublicAccess": True,
        "subnetIds": [
            aws_ec2_subnet_for_eks_worker.get("resource_id"),
            aws_ec2_subnet_for_eks.get("resource_id"),
        ],
        "securityGroupIds": [aws_security_group.get("resource_id")],
    }

    # update eks cluster with test flag
    ret = await hub.states.aws.eks.cluster.present(
        test_ctx,
        name=cluster_name,
        resource_id=resource_id,
        role_arn=arn,
        version=new_version,
        resources_vpc_config=new_resources_vpc_config,
        logging=new_logging,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would update aws.eks.cluster '{cluster_name}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_cluster(
        resource,
        cluster_name,
        arn,
        new_version,
        new_resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        new_logging,
        new_tags,
    )

    # update eks cluster version and tags in real
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        resource_id=resource_id,
        role_arn=arn,
        resources_vpc_config=resources_vpc_config,
        version=new_version,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = await is_cluster_active(hub, ctx, resource_id, cluster_name)
    assert_cluster(
        resource,
        cluster_name,
        arn,
        new_version,
        resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        new_tags,
    )

    # update eks cluster resources_vpc_config in real
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        resource_id=resource_id,
        role_arn=arn,
        resources_vpc_config=new_resources_vpc_config,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = await is_cluster_active(hub, ctx, resource_id, cluster_name)
    assert_cluster(
        resource,
        cluster_name,
        arn,
        new_version,
        new_resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        logging,
        new_tags,
    )

    # update eks cluster logging in real
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        resource_id=resource_id,
        role_arn=arn,
        resources_vpc_config=new_resources_vpc_config,
        logging=new_logging,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = await is_cluster_active(hub, ctx, resource_id, cluster_name)
    assert_cluster(
        resource,
        cluster_name,
        arn,
        new_version,
        new_resources_vpc_config,
        kubernetes_network_config,
        encryption_config,
        new_logging,
        new_tags,
    )

    # Delete cluster with test flag
    ret = await hub.states.aws.eks.cluster.absent(
        test_ctx, name=cluster_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )

    # Delete cluster
    ret = await hub.states.aws.eks.cluster.absent(
        ctx, name=cluster_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )

    # Deleting cluster again should be a no-op
    ret = await hub.states.aws.eks.cluster.absent(
        ctx, name=cluster_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )


async def is_cluster_active(hub, ctx, resource_id, name):
    time.sleep(30)
    for i in range(20):
        resource = await hub.exec.boto3.client.eks.describe_cluster(
            ctx, name=resource_id
        )
        if resource["result"] and resource["ret"]:
            cluster = resource["ret"]["cluster"]
            status = cluster["status"]
            if status != "ACTIVE":
                time.sleep(180)
            else:
                return hub.tool.aws.eks.conversion_utils.convert_raw_cluster_to_present(
                    raw_resource=cluster, idem_resource_name=name
                )
                break


def assert_cluster(
    resource,
    cluster_name,
    arn,
    version,
    resources_vpc_config,
    kubernetes_network_config,
    encryption_config,
    logging,
    tags,
):
    assert cluster_name == resource.get("name")
    assert arn == resource.get("role_arn")
    assert version == resource.get("version")
    assert (
        resources_vpc_config["endpointPrivateAccess"]
        == resource.get("resources_vpc_config")["endpointPrivateAccess"]
    )
    assert (
        resources_vpc_config["endpointPublicAccess"]
        == resource.get("resources_vpc_config")["endpointPublicAccess"]
    )
    assert 2 == len(resource.get("resources_vpc_config")["subnetIds"])
    assert (
        resources_vpc_config["securityGroupIds"]
        == resource.get("resources_vpc_config")["securityGroupIds"]
    )

    assert kubernetes_network_config == resource.get("kubernetes_network_config")
    assert encryption_config == resource.get("encryption_config")
    assert logging == resource.get("logging")
    assert tags == resource.get("tags")
    if resource.get("resource_id") != "resource_id_known_after_present":
        assert resource.get("endpoint")
        assert resource.get("certificate_authority")
        assert resource.get("identity")
        assert resource.get("platform_version")


@pytest.mark.asyncio
async def test_eks_cluster_absent_with_none_resource_id(hub, ctx):
    cluster_name = "idem-test-cluster-" + str(uuid.uuid4())
    # Delete eks cluster with resource_id as None. Result in no-op.
    ret = await hub.states.aws.eks.cluster.absent(
        ctx, name=cluster_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.eks.cluster", name=cluster_name
        )[0]
        in ret["comment"]
    )
