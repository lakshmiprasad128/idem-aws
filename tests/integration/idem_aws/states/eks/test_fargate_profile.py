import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "Localstack pro is unable to create EKS resources, so test against a real AWS endpoint",
)
@pytest.mark.asyncio
async def test_eks_fargate_profile(
    hub, ctx, aws_eks_cluster_fargate, aws_iam_fargate_role_assignment
):
    selectors = [
        {
            "namespace": "default",
        },
    ]
    resource_type = "aws.eks.fargate_profile"
    cluster_name = aws_eks_cluster_fargate.get("name")
    fargate_profile_name = "idem-test-fargate-profile-" + str(uuid.uuid4())
    resource_id = fargate_profile_name
    pod_arn = aws_iam_fargate_role_assignment["arn"]

    # create fargate profile with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.eks.fargate_profile.present(
        test_ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )
    new_state = ret.get("new_state")
    assert fargate_profile_name == new_state.get("name")
    assert cluster_name == new_state.get("cluster_name")
    assert pod_arn == new_state.get("pod_execution_role_arn")
    assert selectors == new_state.get("selectors")

    # create fargate profile on real aws env
    ret = await hub.states.aws.eks.fargate_profile.present(
        ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )
    new_state = ret.get("new_state")
    assert fargate_profile_name == new_state.get("name")
    assert resource_id == new_state.get("resource_id")
    assert cluster_name == new_state.get("cluster_name")
    assert pod_arn == new_state.get("pod_execution_role_arn")
    assert selectors == new_state.get("selectors")

    # create existing resource with resource_id on a real aws env
    ret = await hub.states.aws.eks.fargate_profile.present(
        ctx,
        name=fargate_profile_name,
        resource_id=resource_id,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"aws.eks.fargate_profile '{fargate_profile_name}' already exists"
        in ret["comment"]
    )

    # describe resource
    ret = await hub.states.aws.eks.fargate_profile.describe(ctx)
    state_id = fargate_profile_name + "-" + cluster_name
    assert state_id in ret
    assert "aws.eks.fargate_profile.present" in ret.get(state_id)
    described_resource = ret.get(state_id).get("aws.eks.fargate_profile.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert fargate_profile_name == described_resource_map.get("name")
    assert resource_id == described_resource_map.get("resource_id")
    assert cluster_name == described_resource_map.get("cluster_name")
    assert pod_arn == described_resource_map.get("pod_execution_role_arn")
    assert selectors == described_resource_map.get("selectors")

    # delete resource with no resource_id and with test flag
    ret = await hub.states.aws.eks.fargate_profile.absent(
        test_ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )

    # delete resource with test flag
    ret = await hub.states.aws.eks.fargate_profile.absent(
        test_ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    old_state = ret.get("old_state")
    assert fargate_profile_name == old_state.get("name")
    assert resource_id == old_state.get("resource_id")
    assert cluster_name == old_state.get("cluster_name")
    assert pod_arn == old_state.get("pod_execution_role_arn")
    assert selectors == old_state.get("selectors")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )

    # delete resource on real aws env
    ret = await hub.states.aws.eks.fargate_profile.absent(
        ctx,
        resource_id=resource_id,
        name=fargate_profile_name,
        cluster_name=cluster_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    old_state = ret.get("old_state")
    assert fargate_profile_name == old_state.get("name")
    assert resource_id == old_state.get("resource_id")
    assert cluster_name == old_state.get("cluster_name")
    assert pod_arn == old_state.get("pod_execution_role_arn")
    assert selectors == old_state.get("selectors")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )

    # delete an already existing resource
    ret = await hub.states.aws.eks.fargate_profile.absent(
        ctx,
        resource_id=resource_id,
        name=fargate_profile_name,
        cluster_name=cluster_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=resource_type, name=fargate_profile_name
        )[0]
        in ret["comment"]
    )
